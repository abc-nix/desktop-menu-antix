
For duplication it seems that "layout" and "default layout" are the same thing. As there is only "layout" specified in the xdg menu file. Yet when the menu is parsed via xml parser it matches both "layout" and "default layout". Maybe a not so strict regex is causing this. Commenting out one or the other options (lines 70[3-5] or 70[6-8]) seems to address the duplication, though this is probably not a correct solution.
`elif tag == 'Layout':
     if num_children > 1:
         parent.Layout = self.parse_layout(child)
 elif tag == 'DefaultLayout':
     if num_children > 1:
         parent.DefaultLayout = self.parse_layout(child)`

It is also possible to stop the duplicated menu entries by commenting out lines 739-743 
`elif tag == "Merge":
     layout.order.append([
     "Merge",
         child.attrib.get("type", "all")
     ])`

Also removing the <Merge type=.... lines from the menu.xml file will stop duplication but not maintain the layout.
